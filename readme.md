# Python proof of concept for ADS server implementation

THIS IS VERY ROUGH IMPLEMENTATION IN PYTHON. MOST PEOPLE WILL PREFER C++ IMPLEMENTATION CppAds.

As I had plans for some C++ Beckhoff ADS server implementation, I wanted to check if I'm able to at least write something that will communicate with my Beckhoff CX9000 PLC.

This is exactly it - a VERY ROUGH, VERY PROOF OF CONCEPT implementation of ADS server written in Python. It may be useful for somebody who would like to write their own implementation, but apart from that, it's only kept here for my personal future reference.

## How to run it
To see it in action, run it with "python pyAds.py". This will start the server itself. Also Beckhoff PLC or Twincat PC has to be configured to connect to that.

My Twincat IO tree looks something like that:

- I/O-Configuration
    - I/O Devices
        - Device 1 (Virtual-Ethernet)
            - Box 1 (BK9000)
                - Term 1 (KL1408)
                - Term 2 (KL2408)
                - End Term

This creates input and output modules with bus coupler over ADS running over ethernet. Remember to add at least one linked variable to input or output to keep Twincat happy. Also IP of our PC running pyAds has to entered in Box 1->IP Address tab. AMS Address is the same as IP, but with ".1.1" at the end.
## What to expect if it works
Twincat has a factory defined variable MissedCnt created for each BK9000 module. It's not a real variable on the BK itself, but it's only counting missed frames. If the pyAds works correctly and everything is configured correctly, this variable will not increment (if it's equal to 0xFFFF reset it with right click, Online Write..., enter 0, OK).

PyAds copies bits from output module to input module, so any write on output will be reflected on input.
## Limitations
- Many
- Fully supports only ReadWrite Ads command. Turns out my PLC uses only that.
- Buggy, many things hardcoded.

## License
TLDR: You can do anything you want with it.

Copyright (c) 2016 Jerzy Mikucki

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
